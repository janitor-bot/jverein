jverein (2.8.19+dfsg-1) unstable; urgency=medium

  [ Mechtilde ]
  * [23c84f7] Improve d/watch for the new place of the upstream code
  * [e9e105c] New upstream version 2.8.19+dfsg
  * [40ff11e] Update authors and years
  * [196cb69] Bumped standard version - no changes needed
  * [d4e7191] Improved README.md for new upstream version
  * [94e5baa] Removed unknown field URL

 -- Mechtilde Stehmann <mechtilde@debian.org>  Mon, 03 Oct 2022 19:20:56 +0200

jverein (2.8.18+git20200921.6212a59+dfsg-7) unstable; urgency=medium

  * [898b6cb] Added runtime dependency libmail-java 
              it doesn't come with ${misc}Depends (Closes: 988929)
              so the link should work

 -- Mechtilde Stehmann <mechtilde@debian.org>  Mon, 31 May 2021 21:11:16 +0200

jverein (2.8.18+git20200921.6212a59+dfsg-6) unstable; urgency=medium

  * [0fb9fe3] Bumped standard version in d/control
  * [e5cd2c7] Removed unused links. Added link to /usr/bin (Closes: 988929)
  * [f9b1f92] Improved path to icon in d/j*.install
  * [5efad9c] Improved name of executable
  * [ad9a0ec] Improved  d/j*.lintian-overrides
  * [1df7f13] Added required manpage

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 23 May 2021 09:18:27 +0200

jverein (2.8.18+git20200921.6212a59+dfsg-5) unstable; urgency=medium

  * source upload only

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 25 Apr 2021 07:52:28 +0200

jverein (2.8.18+git20200921.6212a59+dfsg-4) unstable; urgency=medium

  [ Mechtilde ]
  * [d86a379] Improved README.md          Thanks Bernhard Reiter
  * [f816baf] Added content of the Merge Request - thanks Bernhard Reiter
  * [4ebf81f] Changed path to jameica and hibiscus to be reproducible at buildd

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 24 Apr 2021 11:41:42 +0200

jverein (2.8.18+git20200921.6212a59+dfsg-3) unstable; urgency=medium

  * [b7a57da] Added another part to patch file

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 15 Nov 2020 21:02:11 +0100

jverein (2.8.18+git20200921.6212a59+dfsg-2) unstable; urgency=medium

  [ Mechtilde ]
  * [aa3c8fb] Improved d/rules to avoid FTBFS at buildd (Closes: #974773)

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 15 Nov 2020 13:55:15 +0100

jverein (2.8.18+git20200921.6212a59+dfsg-1) unstable; urgency=medium

  [ Mechtilde ]
  * [9ec6e25] Maked d/watch better readable
  * [1bff5c6] Renamed d/jameica-icon.png to d/jverein-icon.png
  * [8489501] Renamed the name of the icon
  * [d343bf0] Changed name of the Icon to install
  * [cce3532] Added license for the new icon with CC0-1.0

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sat, 14 Nov 2020 19:09:58 +0100

jverein (2.8.18+git20200921.6212a59+dfsg-1~exp1) experimental; urgency=medium

  [ Mechtilde ]
  * Initial commit (Closes: #929477)
  * [655f0c8] Added desktop integration
  * [60477d7] Added some more files to be excluded
  * [8651fed] Added d/jverein.examples with some templates
  * [b15db16] Removed empty file d/jverein.docs
  * [531d200] New upstream version 2.8.18+git20200921.6212a59+dfsg
  * [7bc0dd9] Bumpd debhelper to version 13 in d/control
  * [920aad6] Reviewed all copyright entries
  * [517f863] Added two files to install
  * [bc32f00] Added file d/s/include-binariesfor desktop icon
  * [d02059d] ds repaced by dfsg in d/watch

 -- Mechtilde Stehmann <mechtilde@debian.org>  Sun, 04 Oct 2020 17:38:39 +0200
